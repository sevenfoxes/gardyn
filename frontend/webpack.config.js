const HtmlWebPackPlugin = require("html-webpack-plugin");
const ReactRefreshPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const path = require("path");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const modules = path.resolve("node_modules");
const isDev = process.env.NODE_ENV !== 'production';

module.exports = {
  devServer: {
    port: 8090,
    historyApiFallback: {
      disableDotRule: true,
    },
    devMiddleware: {
      writeToDisk: true
    },
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers":
        "X-Requested-With, content-type, Authorization",
    },
  },
  devtool: "cheap-source-map",
  output: {
    path: path.resolve('../static/'),
    publicPath: "/static/",
    filename: 'bundle.js',
    clean: true
  },
  mode: isDev ? 'development' : 'production',
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: modules,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env", ["@babel/preset-react", {"runtime": "automatic"}]],
            plugins: [
              "@babel/plugin-transform-runtime",
              "@babel/plugin-proposal-class-properties",
              isDev && require.resolve('react-refresh/babel')
            ].filter(Boolean)
          }
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
          },
        ],
      },
      {
        test: /\.(jpe?g|svg|png|gif|ico)$/i,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
            },
          },
        ],
      },
    ],
  },
  resolve: {
    alias: {
      "lodash-es": "lodash",
      "lodash.debounce": "lodash/debounce",
      ghostwheel$: path.resolve(__dirname, "src/ghostwheel/"),
      components$: path.resolve(__dirname, "src/components/"),
    },
  },
  plugins: [
    new CleanWebpackPlugin({
      dry: false,
      cleanStaleWebpackAssets: true,
      cleanAfterEveryBuildPatterns: ['hot-update*.*'],
    }),
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html",
      favicon: "src/images/favicon.ico",
    }),
    isDev && new ReactRefreshPlugin({
      overlay: {
        sockPort: 8090,
      },
    }),
  ].filter(Boolean),
};
