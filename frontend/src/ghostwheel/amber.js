import { atom, selectorFamily } from "recoil";

const amber = atom({
  key: 'amber',
  default: {}
});

export const amberSelector = selectorFamily({
  key: 'amberSelector',
  get: path => ({ get }) => get(amber)[path],
  set: path => ({ get, set }, newData) => {
    set(amber, {...get(amber), [path]: newData
      .reduce((acc, item) => ({...acc, [item.id]: item }),{}) })
  }
})
