import { useEffect } from "react";
import { useSetSelector } from "ghostwheel";
import { pageState } from "./atom";
import { useRecoilValue } from 'recoil';


export const usePage = atom => {
  const setPage = useSetSelector(pageState);
  const { route, ...data } = useRecoilValue(atom);
  useEffect(() => {
    setPage(data);
  }, [setPage, data]);
};
