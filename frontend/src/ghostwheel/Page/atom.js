import { amber } from "ghostwheel";

export const pageState = amber({
  "key": "pageState",
  default: {
    name: "",
    title: "",
  },
});
