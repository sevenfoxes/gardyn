import { createUseStyles } from "react-jss";
export const makeStyles = (s, name) => createUseStyles(s, { name });
