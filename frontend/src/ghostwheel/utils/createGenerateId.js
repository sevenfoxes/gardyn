// this function may not be used, this is a fix for material ui, but we're not using it, so...
export const createGenerateId = () => {
  let ruleCounter = 0;

  return (rule, styleSheet) => {
    ruleCounter += 1;

    const suffix = `${rule.key}-${ruleCounter}`;

    // Help with debuggability.
    if (styleSheet.options.classNamePrefix) {
      return `${styleSheet.options.classNamePrefix}-${suffix}`;
    }

    return `${suffix}`;
  };
};
