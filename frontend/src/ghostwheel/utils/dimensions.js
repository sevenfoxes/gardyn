import useReactDimensions from "use-react-dimensions";

export const useDimensions = useReactDimensions
