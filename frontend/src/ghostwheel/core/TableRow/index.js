import { classnames, makeStyles } from "ghostwheel";

export const useStyles = makeStyles({
  root: {},
}, "TableRow");

export const TableRow = ({ className, children, ...props }) => {
  const c = useStyles();

  return (
    <tr className={classnames(c.root, className)} {...props}>
      {children}
    </tr>
  );
};
