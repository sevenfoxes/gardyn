import { classnames, makeStyles } from "ghostwheel";

const useStyles = makeStyles(({ variables, neonGreen, brightGreen, yellow, Color }) => ({
  root: {
    alignItems: 'center',
    background: 'transparent',
    border: 0,
    color: brightGreen,
    cursor: 'pointer',
    display: 'flex',
    padding: 0,
    position: 'relative',

    '&:hover': {
      color: yellow,
    },

    '& svg': {
      height: 20,
      width: 28
    }
  },
}), 'Button');

export const Button = ({ styling , children }) => {
  const c = useStyles();
  return (
    <button className={classnames(c.root, { [styling?.root]: styling?.root})}>
      {children}
    </button>
  )
}
