import { makeStyles } from "ghostwheel";

const useStyles = makeStyles(({ variables }) => ({
  root: {
    ...variables.wrapper,
    alignItems: 'inherit',
    paddingBottom: ({ vPadding }) => vPadding,
    paddingTop: ({ vPadding }) => vPadding,
  },
}), "Wrapper");

export const Wrapper = ({ children, vPadding = 0 }) => {
  const c = useStyles({ vPadding });

  return (
    <div className={c.root}>
      {children}
    </div>
  );
};
