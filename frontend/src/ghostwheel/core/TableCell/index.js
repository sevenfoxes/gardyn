import { classnames, makeStyles } from "ghostwheel";
import { variables } from "../../../theme";

const useStyles = makeStyles({
  root: {
    padding: variables.cellPadding,
    minHeight: 68,
  },
  currency: {
    textAlign: "right",
  },
}, "TableCell");

export const TableCell = ({ children, currency, ...props }) => {
  const c = useStyles();

  return (
    <td className={classnames(c.root, props.className, !!currency && c.currency)} {...props}>
      {children}
    </td>
  );
};
