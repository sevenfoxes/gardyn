import { isDev, local, prod } from "../config";
import { log } from 'ghostwheel';
import { objectToQueryString } from "./utils";

const host = isDev ? local : prod;

const request = async (url, params = { format: 'json' }, method = 'GET') => {
  const isGet = method === 'GET';
  let rUrl;

  if (isGet && params) {
    rUrl = `${url}?${objectToQueryString(params)}`;
  }

  if (!isGet && params) {
    rUrl = options.body = JSON.stringify(params);
  }

  const options = {
    method,
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const response = await fetch(host + url, options);

  if (response.status !== 200) {
    return log.error('Hmm, it appears that things are... NOT OK!')
  }

  const result = await response.json();

  return result;
}

const get = async (url, params) => request(url, params);

const create = async (url, params) => request(url, params, 'POST');

const update = async (url, params) => request(url, params, 'PUT')

const remove = async (url, params) => request(url, params, 'DELETE');

export const shadow = { get, create, update, remove }
