from django.db import models
from enum import Enum
from django.utils.translation import gettext_lazy as _
from django.utils import timezone


class Garden(models.Model):
    class SubstrateChoices(models.TextChoices):
        HYDROTON = 'HT', _('hydroton')
        COCO_COIR = 'CC', _('Coco Coir')
        ROCKWOOL = 'RW', _('Rockwool')
        SOIL = 'SO', _('Soil')
        OTHER = 'OT', _('Other')

    class GrowTechniques(models.TextChoices):
        DRIP = 'TD', _('Top Drip')
        DWC = 'DW', _('Deep Water Culture')
        NFT = 'NF', _('Nutrient Film Technique')
        KRAKY = 'KR', _('Kraky')
        EBB_FLOW = 'EF', _('Ebb and Flow')
        AERO = 'AP', _('Aeroponics')
        HYBRID = 'HY', _('Hybrid')
        OTHER = 'OT', _('Other')

    name = models.CharField(max_length=200)
    water_capacity = models.PositiveIntegerField()
    water_supply = models.BooleanField()

    substrate = models.CharField(
        max_length=2,
        choices=SubstrateChoices.choices,
        default=SubstrateChoices.HYDROTON
    )

    grow_technique = models.CharField(
        max_length=2,
        choices=GrowTechniques.choices,
        default=GrowTechniques.DRIP
    )

    length = models.PositiveIntegerField()
    width = models.PositiveIntegerField()
    updated_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name
