from rest_framework import serializers
from . import models


class GardenSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'name', 'water_capacity', 'water_supply',
                  'substrate', 'grow_technique', 'updated_at',)
        model = models.Garden
