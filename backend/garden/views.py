from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ModelViewSet
from .models import Garden
from .serializers import GardenSerializer
from django.views.decorators.cache import never_cache
from django.views.generic import TemplateView

index = never_cache(TemplateView.as_view(template_name='index.html'))


class GardenViewSet(ModelViewSet):
    queryset = Garden.objects.all()
    serializer_class = GardenSerializer
    permission_classes = [AllowAny]
