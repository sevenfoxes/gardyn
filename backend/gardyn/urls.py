from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework import routers
from garden import views

router = routers.DefaultRouter()
router.register(r'gardens', views.GardenViewSet, basename='index')

urlpatterns = [
    path('', include('garden.urls')),
    path('api/', include(router.urls)),
    path('admin/', admin.site.urls),
]
