# Gardyn

know what's happening in your gardens

## Setup the environment

You will need to have `docker`, `docker-compose` , and `pipenv`

- make a `.env` file and adjust the contents:
  `cp .env.example .env`

- run `pipenv install`
- activate the virtual environment with
  `pipenv shell`

- then run
  `docker-compose up`

- to work on the frontend open a second shell and run the following commands
  `cd /frontend`
  `npm install`
  `npm start`

## configure/prepare your raspberry pi

hop over to the gardyn-sense repo and follow the install directions

## deployment

...and then it's on unraid!
